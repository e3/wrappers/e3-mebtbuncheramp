importPackage(Packages.org.csstudio.opibuilder.scriptUtil);

var pvMag = PVUtil.getDouble(pvArray[0]);
var pvAlrm = PVUtil.getDouble(pvArray[1]);
var pvWrn = PVUtil.getDouble(pvArray[2]);

widget.setPropertyValue("foreground_color",ColorFontUtil.getColorFromRGB(255,255,255));
if (pvAlrm >= pvMag){
	widget.setPropertyValue("background_color",ColorFontUtil.getColorFromRGB(255,0,0));
	}
else if (pvWrn >= pvMag)
	{
	widget.setPropertyValue("background_color",ColorFontUtil.getColorFromRGB(255,128,0));
	}
else
	{
	widget.setPropertyValue("background_color",ColorFontUtil.getColorFromRGB(255,255,255));
	}

// reload the embedded OPI
var currentOpi = widgetController.getPropertyValue("opi_file")
widgetController.setPropertyValue("opi_file", "");
widgetController.setPropertyValue("opi_file", currentOpi);
