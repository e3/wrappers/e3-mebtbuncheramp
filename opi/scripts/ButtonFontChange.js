importPackage(Packages.org.csstudio.opibuilder.scriptUtil);

var pv = PVUtil.getDouble(pvArray[0]);


if (pv < 1){
	widget.setPropertyValue("foreground_color",ColorFontUtil.getColorFromRGB(100,200,100));
	}
else
	{
	widget.setPropertyValue("foreground_color",ColorFontUtil.getColorFromRGB(0,0,0));
	}

// reload the embedded OPI
var currentOpi = widgetController.getPropertyValue("opi_file")
widgetController.setPropertyValue("opi_file", "");
widgetController.setPropertyValue("opi_file", currentOpi);
