importPackage(Packages.org.csstudio.opibuilder.scriptUtil);

var pv = PVUtil.getDouble(pvArray[0]);


if (pv < 1){
	widget.setPropertyValue("border_color",ColorFontUtil.getColorFromRGB(255,0,0));
	widget.setPropertyValue("border_width",7);

	}
else
	{
    widget.setPropertyValue("border_color",ColorFontUtil.getColorFromRGB(0,100,0));
    widget.setPropertyValue("border_width",7);
	}

// reload the embedded OPI
var currentOpi = widgetController.getPropertyValue("opi_file")
widgetController.setPropertyValue("opi_file", "");
widgetController.setPropertyValue("opi_file", currentOpi);
