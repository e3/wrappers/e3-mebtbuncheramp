importPackage(Packages.org.csstudio.opibuilder.scriptUtil);

var pv = PVUtil.getDouble(pvArray[0]);

widget.setPropertyValue("image_file","Images/SSPA_02_SSPADG_COLB.png");
var pvPS = PVUtil.getDouble(pvs[0]);  // PSStat
var pvINTL = PVUtil.getDouble(pvs[1]);// SSPAIntlock
var pvALRM = PVUtil.getDouble(pvs[2]);// SSPASelfProtAlrm
var pvSTAT = PVUtil.getDouble(pvs[3]);// SSPAStat
var pvCOL = PVUtil.getDouble(pvs[4]); // ColStat

switch(pvCOL)
{
	case 0:
	{
	if((pvSTAT==3) || (pvINTL==0) || (pvALRM==0))
		widget.setPropertyValue("image_file","Images/SSPA_02_SSPAR_COLB.png");
	else if(pvPS==2)
		widget.setPropertyValue("image_file","Images/SSPA_02_SSPAY_COLB.png");
	else if(pvSTAT==1)
		widget.setPropertyValue("image_file","Images/SSPA_02_SSPACG_COLB.png");
	else
		widget.setPropertyValue("image_file","Images/SSPA_02_SSPADG_COLB.png");
	}
	break;
	case 1:
	{
	if((pvSTAT==3) || (pvINTL==0) || (pvALRM==0))
		widget.setPropertyValue("image_file","Images/SSPA_02_SSPAR_COLY.png");
	else if(pvPS==2)
		widget.setPropertyValue("image_file","Images/SSPA_02_SSPAY_COLY.png");
	else if(pvSTAT==1)
		widget.setPropertyValue("image_file","Images/SSPA_02_SSPACG_COLY.png");
	else
		widget.setPropertyValue("image_file","Images/SSPA_02_SSPADG_COLY.png");
	}
	break;
	default:
	{
	if((pvSTAT==3) || (pvINTL==0) || (pvALRM==0))
		widget.setPropertyValue("image_file","Images/SSPA_02_SSPAR_COLO.png");
	else if(pvPS==2)
		widget.setPropertyValue("image_file","Images/SSPA_02_SSPAY_COLO.png");
	else if(pvSTAT==1)
		widget.setPropertyValue("image_file","Images/SSPA_02_SSPACG_COLO.png");
	else
		widget.setPropertyValue("image_file","Images/SSPA_02_SSPADG_COLO.png");


	}
}
