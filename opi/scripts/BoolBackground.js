importPackage(Packages.org.csstudio.opibuilder.scriptUtil);

var pv = PVUtil.getDouble(pvArray[0]);
//widget.setPropertyValue("border_width",20);

//	widget.setPropertyValue("border_color",ColorFontUtil.getColorFromRGB(0,10,0));


if (pv < 4){
	widget.setPropertyValue("background_color",ColorFontUtil.getColorFromRGB(0,255,255));
	widget.setPropertyValue("border_color",ColorFontUtil.getColorFromRGB(0,10,0));
	widget.setPropertyValue("border_width",5);

	}
else if (pv > 4)
	{
	widget.setPropertyValue("background_color",ColorFontUtil.getColorFromRGB(255,255,0));
    widget.setPropertyValue("border_color",ColorFontUtil.getColorFromRGB(0,255,255));
    widget.setPropertyValue("border_width",10);

	}
else
	{
	widget.setPropertyValue("background_color",ColorFontUtil.getColorFromRGB(255,255,255));
    widget.setPropertyValue("border_color",ColorFontUtil.getColorFromRGB(0,255,255));
    widget.setPropertyValue("border_width",10);

	}
// reload the embedded OPI
//var currentOpi = widgetController.getPropertyValue("opi_file")
//widgetController.setPropertyValue("opi_file", "");
//widgetController.setPropertyValue("opi_file", currentOpi);
