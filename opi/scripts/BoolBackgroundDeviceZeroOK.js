importPackage(Packages.org.csstudio.opibuilder.scriptUtil);

var pv = PVUtil.getDouble(pvArray[0]);

widget.setPropertyValue("foreground_color",ColorFontUtil.getColorFromRGB(255,255,255));
if (pv < 1){
	widget.setPropertyValue("background_color",ColorFontUtil.getColorFromRGB(0,100,0));
	}
else
	{
	widget.setPropertyValue("background_color",ColorFontUtil.getColorFromRGB(255,0,0));
	}

// reload the embedded OPI
var currentOpi = widgetController.getPropertyValue("opi_file")
widgetController.setPropertyValue("opi_file", "");
widgetController.setPropertyValue("opi_file", currentOpi);
